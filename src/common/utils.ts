import { BiomDisplayFormat, DataItem, RowItem } from "./types";
import { MINIMAL_RELATIVE_ABUNDANCE, STRAIN_LEVEL } from "./constants";

export const parseToDisplayFormat = (
  data: DataItem[],
  rows: RowItem[]
): BiomDisplayFormat[] =>
  rows.map((row, index) => ({
    name: row.metadata.lineage[STRAIN_LEVEL]?.name || "",
    taxId: row.metadata.lineage[STRAIN_LEVEL]?.tax_id || 0,
    relativeAbundance: formatPercentageValue(data[index * 3][2]),
    abundanceScore: data[index * 3 + 1][2].toFixed(2),
    uniqueMatchesFrequency: data[index * 3 + 2][2].toFixed(0),
  }));

const formatPercentageValue = (value: number) =>
  value < MINIMAL_RELATIVE_ABUNDANCE
    ? "<0.01%"
    : `${(value * 100).toFixed(2)}%`;
