type LineageItem = {
  rank: string;
  name: string;
  tax_id: number;
};

export type RowItem = {
  id: string;
  metadata: {
    taxonomy: [string, string, string, string, string, string, string, string];
    tax_id: number;
    title: string;
    lineage: [
      LineageItem,
      LineageItem,
      LineageItem,
      LineageItem,
      LineageItem,
      LineageItem,
      LineageItem,
      LineageItem
    ];
    id: string;
    assembly?: string;
  };
};

export type DataItem = [number, number, number];

export type BiomDisplayFormat = {
  name: string;
  taxId: number;
  abundanceScore: string;
  relativeAbundance: string;
  uniqueMatchesFrequency: string;
};
