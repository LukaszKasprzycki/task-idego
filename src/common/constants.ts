export enum FetchStatus {
  PENDING = "PENDING",
  SUCCESS = "SUCCESS",
  FAILURE = "FAILURE",
}

export const MINIMAL_RELATIVE_ABUNDANCE = 0.01;

export const STRAIN_LEVEL = 7;
