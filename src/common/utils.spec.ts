import { BiomDisplayFormat, DataItem, RowItem } from "./types";
import { parseToDisplayFormat } from "./utils";

const input: {
  rows: RowItem[];
  data: DataItem[];
} = {
  rows: [
    {
      id: "Lactobacillus crispatus SJ 3C US",
      metadata: {
        taxonomy: [
          "Bacteria",
          "Firmicutes",
          "Bacilli",
          "Lactobacillales",
          "Lactobacillaceae",
          "Lactobacillus",
          "Lactobacillus crispatus",
          "Lactobacillus crispatus SJ-3C-US",
        ],
        tax_id: 575598,
        title: "Lactobacillus crispatus SJ-3C-US",
        lineage: [
          {
            rank: "superkingdom",
            name: "Bacteria",
            tax_id: 2,
          },
          {
            rank: "phylum",
            name: "Firmicutes",
            tax_id: 1239,
          },
          {
            rank: "class",
            name: "Bacilli",
            tax_id: 91061,
          },
          {
            rank: "order",
            name: "Lactobacillales",
            tax_id: 186826,
          },
          {
            rank: "family",
            name: "Lactobacillaceae",
            tax_id: 33958,
          },
          {
            rank: "genus",
            name: "Lactobacillus",
            tax_id: 1578,
          },
          {
            rank: "species",
            name: "Lactobacillus crispatus",
            tax_id: 47770,
          },
          {
            rank: "strain",
            name: "Lactobacillus crispatus SJ-3C-US",
            tax_id: 575598,
          },
        ],
        id: "Lactobacillus crispatus SJ 3C US",
      },
    },
    {
      id: "Lactobacillus sp UMNPBX1 GCA 002554085.1",
      metadata: {
        taxonomy: [
          "Bacteria",
          "Firmicutes",
          "Bacilli",
          "Lactobacillales",
          "Lactobacillaceae",
          "Lactobacillus",
          "Lactobacillus_u_s",
          "Lactobacillus sp. UMNPBX1",
        ],
        tax_id: 2042046,
        title: "Lactobacillus sp. UMNPBX1",
        lineage: [
          {
            rank: "superkingdom",
            name: "Bacteria",
            tax_id: 2,
          },
          {
            rank: "phylum",
            name: "Firmicutes",
            tax_id: 1239,
          },
          {
            rank: "class",
            name: "Bacilli",
            tax_id: 91061,
          },
          {
            rank: "order",
            name: "Lactobacillales",
            tax_id: 186826,
          },
          {
            rank: "family",
            name: "Lactobacillaceae",
            tax_id: 33958,
          },
          {
            rank: "genus",
            name: "Lactobacillus",
            tax_id: 1578,
          },
          {
            rank: "species",
            name: "Lactobacillus_u_s",
            tax_id: 1578,
          },
          {
            rank: "strain",
            name: "Lactobacillus sp. UMNPBX1",
            tax_id: 2042046,
          },
        ],
        id: "Lactobacillus sp UMNPBX1 GCA 002554085.1",
        assembly: "GCA_002554085.1",
      },
    },
    {
      id: "Capnocytophaga sputigena ATCC 33612",
      metadata: {
        taxonomy: [
          "Bacteria",
          "Bacteroidetes",
          "Flavobacteriia",
          "Flavobacteriales",
          "Flavobacteriaceae",
          "Capnocytophaga",
          "Capnocytophaga sputigena",
          "Capnocytophaga sputigena ATCC 33612",
        ],
        tax_id: 553177,
        title: "Capnocytophaga sputigena ATCC 33612",
        lineage: [
          {
            rank: "superkingdom",
            name: "Bacteria",
            tax_id: 2,
          },
          {
            rank: "phylum",
            name: "Bacteroidetes",
            tax_id: 976,
          },
          {
            rank: "class",
            name: "Flavobacteriia",
            tax_id: 117743,
          },
          {
            rank: "order",
            name: "Flavobacteriales",
            tax_id: 200644,
          },
          {
            rank: "family",
            name: "Flavobacteriaceae",
            tax_id: 49546,
          },
          {
            rank: "genus",
            name: "Capnocytophaga",
            tax_id: 1016,
          },
          {
            rank: "species",
            name: "Capnocytophaga sputigena",
            tax_id: 1019,
          },
          {
            rank: "strain",
            name: "Capnocytophaga sputigena ATCC 33612",
            tax_id: 553177,
          },
        ],
        id: "Capnocytophaga sputigena ATCC 33612",
      },
    },
    {
      id: "Capnocytophaga gingivalis ATCC 33624",
      metadata: {
        taxonomy: [
          "Bacteria",
          "Bacteroidetes",
          "Flavobacteriia",
          "Flavobacteriales",
          "Flavobacteriaceae",
          "Capnocytophaga",
          "Capnocytophaga gingivalis",
          "Capnocytophaga gingivalis ATCC 33624",
        ],
        tax_id: 553178,
        title: "Capnocytophaga gingivalis ATCC 33624",
        lineage: [
          {
            rank: "superkingdom",
            name: "Bacteria",
            tax_id: 2,
          },
          {
            rank: "phylum",
            name: "Bacteroidetes",
            tax_id: 976,
          },
          {
            rank: "class",
            name: "Flavobacteriia",
            tax_id: 117743,
          },
          {
            rank: "order",
            name: "Flavobacteriales",
            tax_id: 200644,
          },
          {
            rank: "family",
            name: "Flavobacteriaceae",
            tax_id: 49546,
          },
          {
            rank: "genus",
            name: "Capnocytophaga",
            tax_id: 1016,
          },
          {
            rank: "species",
            name: "Capnocytophaga gingivalis",
            tax_id: 1017,
          },
          {
            rank: "strain",
            name: "Capnocytophaga gingivalis ATCC 33624",
            tax_id: 553178,
          },
        ],
        id: "Capnocytophaga gingivalis ATCC 33624",
      },
    },
  ],
  data: [
    [0, 0, 0.944307],
    [0, 1, 139028.29],
    [0, 2, 1362.0],
    [1, 0, 0.026278],
    [1, 1, 3868.86],
    [1, 2, 3188.0],
    [69, 0, 7.1e-5],
    [69, 1, 10.49],
    [69, 2, 88.0],
    [70, 0, 8.5e-5],
    [70, 1, 12.47],
    [70, 2, 78.0],
  ],
};

const expectedOutput: BiomDisplayFormat[] = [
  {
    name: "Lactobacillus crispatus SJ-3C-US",
    taxId: 575598,
    abundanceScore: "139028.29",
    relativeAbundance: "94.43%",
    uniqueMatchesFrequency: "1362",
  },
  {
    name: "Lactobacillus sp. UMNPBX1",
    taxId: 2042046,
    abundanceScore: "3868.86",
    relativeAbundance: "2.63%",
    uniqueMatchesFrequency: "3188",
  },
  {
    name: "Capnocytophaga sputigena ATCC 33612",
    taxId: 553177,
    abundanceScore: "10.49",
    relativeAbundance: "<0.01%",
    uniqueMatchesFrequency: "88",
  },
  {
    name: "Capnocytophaga gingivalis ATCC 33624",
    taxId: 553178,
    abundanceScore: "12.47",
    relativeAbundance: "<0.01%",
    uniqueMatchesFrequency: "78",
  },
];
describe("parseToDisplayFormat tests", () => {
  it("produces expected values", () => {
    expect(parseToDisplayFormat(input.data, input.rows)).toEqual(
      expectedOutput
    );
  });
});
