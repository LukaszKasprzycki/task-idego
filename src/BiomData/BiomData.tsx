import React, { useEffect, useState } from "react";
import { parseToDisplayFormat } from "../common/utils";
import { BiomDisplayFormat } from "../common/types";
import { FetchStatus } from "../common/constants";
import BiomTable from "./BiomTable/BiomTable";

function BiomData() {
  const [fetchStatus, setFetchStatus] = useState(FetchStatus.PENDING);
  const [tableData, setTableData] = useState<BiomDisplayFormat[] | null>(null);

  useEffect(() => {
    fetch("biom.json")
      .then((response) => response.json())
      .then(({ data, rows }) => parseToDisplayFormat(data, rows))
      .then((dataInTableFormat) => {
        setTableData(dataInTableFormat);
        setFetchStatus(FetchStatus.SUCCESS);
      })
      .catch((_) => {
        setFetchStatus(FetchStatus.FAILURE);
      });
  }, []);

  const render = () => {
    switch (fetchStatus) {
      case FetchStatus.PENDING:
        return <div>...Loading</div>;
      case FetchStatus.SUCCESS:
        return tableData ? (
          <BiomTable data={tableData} />
        ) : (
          <div>No data provided</div>
        );
      case FetchStatus.FAILURE:
        return <div>An error occurred while parsing the JSON data</div>;
    }
  };

  return <>{render()}</>;
}

export default BiomData;
