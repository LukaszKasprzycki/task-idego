import React from "react";
import {
  useReactTable,
  createColumnHelper,
  getCoreRowModel,
  flexRender,
} from "@tanstack/react-table";
import { BiomDisplayFormat } from "../../common/types";
import "./styles.css";

const columnHelper = createColumnHelper<BiomDisplayFormat>();

const columns = [
  columnHelper.accessor("name", {
    header: "Name",
    cell: (info) => info.renderValue(),
  }),
  columnHelper.accessor((row) => row.taxId, {
    header: "Tax ID",
    cell: (info) => info.renderValue(),
  }),
  columnHelper.accessor("abundanceScore", {
    header: "Abundance score",
    cell: (info) => info.renderValue(),
  }),
  columnHelper.accessor("relativeAbundance", {
    header: "Relative abundance",
    cell: (info) => info.renderValue(),
  }),
  columnHelper.accessor("uniqueMatchesFrequency", {
    header: "Unique matches frequency",
    cell: (info) => info.renderValue(),
  }),
];

const BiomTable = ({ data }: { data: BiomDisplayFormat[] }) => {
  const table = useReactTable({
    data: data,
    columns,
    getCoreRowModel: getCoreRowModel(),
  });

  return (
    <table>
      <thead>
        {table.getHeaderGroups().map((headerGroup) => {
          return (
            <tr key={headerGroup.id}>
              {headerGroup.headers.map((header) => (
                <th key={header.id}>
                  {flexRender(
                    header.column.columnDef.header,
                    header.getContext()
                  )}
                </th>
              ))}
            </tr>
          );
        })}
      </thead>
      <tbody>
        {table.getRowModel().rows.map((row) => (
          <tr key={row.id}>
            {row.getVisibleCells().map((cell) => (
              <td key={cell.id}>
                {flexRender(cell.column.columnDef.cell, cell.getContext())}
              </td>
            ))}
          </tr>
        ))}
      </tbody>
    </table>
  );
};

export default BiomTable;
